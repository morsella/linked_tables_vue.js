
var pageInfo ={
	title: 'Capisci Linked Tables'
};
var pageInfoModel = new Vue({
	el:'title',
	data:pageInfo
});
var menuLink = new Vue({
	el: '.top_menu',
	data:{
	    url_menu: 'capisci_designs/menu.jpg'
	}
});
var sideLink = new Vue({
	el: '.side_bar',
	data:{
		url_side_bar: 'capisci_designs/side_bar.jpg'
	}
});

var tableModel = {
    tables_title: "financieel groepering",
    office: '100 - Bovenschools',
    table1_name: 'Kostenplaats - Kostendrager - Organisatie',
    table2_name: 'Gekoppeld',
        unlinkedItems:[
	    {id: 0, name: 'Banans', status: false, visible: true},
	    {id: 1, name: 'Mango', status: false, visible: true},
	    {id: 2, name: 'Avocado', status: false, visible: true},
	    {id: 3, name: 'Grapes', status: false, visible: true},
	    {id: 4, name: 'Apple', status: false, visible: true},
	    {id: 5, name: 'Kiwi', status: false, visible: true},
	    {id: 6, name: 'Ananas', status: false, visible: true},
	    {id: 7, name: 'Strawberries', status: false, visible: true},
	    {id: 8, name: 'Peach', status: false, visible: true},
	    {id: 9, name: 'Plums', status: false, visible: true},
	    {id: 10, name: 'Cherry',status: false, visible: true},
	    {id: 11, name: 'Orange', status: false, visible: true},
	    {id: 12, name: 'Banans', status: false, visible: true},
	    {id: 13, name: 'Mango', status: false, visible: true},
	    {id: 14, name: 'Avocado', status: false, visible: true},
	    {id: 15, name: 'Grapes', status: false, visible: true},
	    {id: 16, name: 'Apple', status: false, visible: true},
	    {id: 17, name: 'Kiwi', status: false, visible: true},
	    {id: 18, name: 'Ananas', status: false, visible: true},
	    {id: 19, name: 'Strawberries', status: false, visible: true},
	    {id: 20, name: 'Peach', status: false, visible: true},
	    {id: 21, name: 'Plums', status: false, visible: true},
	    {id: 22, name: 'Cherry',status: false, visible: true},
	    {id: 23, name: 'Orange', status: false, visible: true}

    ],
    newItems: [],
};
var tableViewModel = new Vue({
    el: '.container',
    data: tableModel,
	methods:{
	    selectItem: function(i){
			var id = this.unlinkedItems[i].id;
		    if (this.unlinkedItems[i].status === false) {
			 	     this.unlinkedItems[i].status = true;
			}else{
				var selectedIndex = this.unlinkedItems.filter(function(item){
					item.id === id;
				});
				this.unlinkedItems[i].status = false;
			}
	    },
	    removeItem: function(i){
			var id = this.newItems[i].id;
		    if (this.newItems[i].status === false) {
			 	     this.newItems[i].status = true;
			}else{
				var selectedIndex = this.newItems.filter(function(item){
					item.id === id;
				});
				this.newItems[i].status = false;
			}
	    },
	    addItems: function(){
			this.unlinkedItems.forEach(function(element) {
				if(element.status === true){
					element.visible = false;
					element.status = false;
					this.newItems.push({id: element.id, name: element.name, status: false, visible: true});
				}
			},this);
	    },
	    removeItems: function() {
			var index = 0;
			this.newItems.forEach(function(element) {
				if(element.status === true){
					element.status = false;
					element.visible = false;
					var item = this.unlinkedItems.find(function(item){
						return (item.id === element.id)
					});
					var index = this.unlinkedItems.indexOf(item);
					this.unlinkedItems[index].visible = true;
				}
			},this);
			this.newItems = this.newItems.filter(function(element){ return element.visible});
		}
},
    computed:{
	   //Show filtered items that visible
	   visibleUnlinkedItems:{
		   		get:function () {
				var filter = Vue.filter('filterBy')
				return filter(this.unlinkedItems, true, 'visible')
			}
		}
	  // Select all items function....
/*
	    selectedStatus:{
		    get:function(){
				return this.newItems.length === this.unlinkedItems.length;
			},
		    set:function(){
				var isChecked = false;
				//check if all items selected
				if(this.newItems.length === this.unlinkedItems.length){
					//empty all items
					this.newItems = [];
				}else{
					//select all items
					this.newItems = this.unlinkedItems.slice(0);
					isChecked = true;
				}
				this.unlinkedItems.forEach(function(element) {
						element.status = isChecked;
				},
				// the last this at the bottom sets the this of the inner function(scope) to the outer scope
				this);

		    }
	    }
*/
	}
});


